<?php 
require_once '../koneksi/conn.php'; 
$query = $conn->query("SELECT * FROM kas_masuk ORDER BY tanggal DESC");
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <!-- <ol class="breadcrumb">
                <li><a href="#">Dana Masuk</a></li>
            </ol> -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
     <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                    </div>
<!--                     <div class="col-sm-6">
                        <button class="btn btn-success btn-sm pull-right" onclick="tambah()">Tambah</button>                    
                        <button class="btn btn-warning btn-sm pull-right" onclick="laporan()" style="margin-right: 5px;">Laporan Dana Masuk</button>
                    </div> -->
                </div>
                    <br>
                    <form  role="form" method="post" action="<?php echo base_url ?>recorddelete.php" class="form-inline">
                        <div class="form-group mb-2">
                            <label class="sr-only"></label>
                        </div>
                        <button class="btn btn-danger btn-sm pull-right" type="submit" name="submitdelete" class="btn btn-danger">Hapus Record</button>
                    </form>

                    <?php
                        session_start();
                        if (isset($_POST['submitform'])) {

                            if(isset($_SESSION['jumlahsiswa'])){

                                $jumlah = $_SESSION['jumlahsiswa'];
                                $nama = array();

                                $nilaiipk = array();
                                $textipk = array();
                                $gapipk = array();
                                $bobotipk = array();

                                $nilaipenghasilan = array();
                                $textpenghasilan = array();
                                $gappenghasilan = array();
                                $bobotpenghasilan = array();

                                $nilaitanggungan = array();
                                $texttanggungan = array();
                                $gaptanggungan = array();
                                $bobottanggungan = array();

                                $nilaismt = array();
                                $textsmt = array();
                                $gapsmt = array();
                                $bobotsmt = array();

                                $ncfsiswa = array();
                                $nsfsiswa = array();
                                $hasilsiswa = array();

                                for($a=1;$a<=$jumlah;$a++) {

                                   $nama[$a] = $_POST['namasiswa'.$a];
                                   $nilaiipk[$a] = $_POST['ipk'.$a];
                                   $nilaipenghasilan[$a] = $_POST['penghasilan'.$a];
                                   $nilaitanggungan[$a] = $_POST['tanggungan'.$a];
                                   $nilaismt[$a] = $_POST['smt'.$a];

                                   $sql = $conn->query("INSERT INTO siswa (nama, ipk, penghasilan, tanggungan, semester) VALUES('$nama[$a]','$nilaiipk[$a]','$nilaipenghasilan[$a]','$nilaitanggungan[$a]','$nilaismt[$a]')");

                                }

                                for($a=1;$a<=$jumlah;$a++) {

                                    if ($nilaiipk[$a] == "1"){
                                        $textipk[$a] = "< 2,5 ";
                                    } elseif ($nilaiipk[$a] == "2") {
                                        $textipk[$a] = ">2,5 dan <= 3";
                                    } elseif ($nilaiipk[$a] == "3") {
                                        $textipk[$a] = ">3 dan <= 3.,5";
                                    } else {
                                        $textipk[$a] = "> 3,5";
                                    }

                                    if ($nilaipenghasilan[$a] == "4"){
                                        $textpenghasilan[$a] = "<=1.000.000";
                                    } elseif ($nilaipenghasilan[$a] == "3") {
                                        $textpenghasilan[$a] = ">1.000.000 - <=3.000.000";
                                    } elseif ($nilaipenghasilan[$a] == "2") {
                                        $textpenghasilan[$a] = ">3.000.000 - <=5.000.000";
                                    } else {
                                        $textpenghasilan[$a] = ">=5.000.000";
                                    }

                                    if ($nilaitanggungan[$a] == "1"){
                                        $texttanggungan[$a] = "Jumlah 1";
                                    } elseif ($nilaitanggungan[$a] == "2") {
                                        $texttanggungan[$a] = "Jumlah 2";
                                    } elseif ($nilaitanggungan[$a] == "3") {
                                        $texttanggungan[$a] = "Jumlah 3";
                                    } else {
                                        $texttanggungan[$a] = "Jumlah >3";
                                    }

                                    if ($nilaismt[$a] == "0"){
                                        $textsmt[$a] = "<=2 / >8";
                                    } elseif ($nilaismt[$a] == "1") {
                                        $textsmt[$a] = "3";
                                    } elseif ($nilaismt[$a] == "2") {
                                        $textsmt[$a] = "4";
                                    } elseif ($nilaismt[$a] == "3") {
                                        $textsmt[$a] = "5, 6";
                                    } else {
                                        $textsmt[$a] = "7, 8";
                                    }

                                    $sql = $conn->query("INSERT INTO keterangansiswa (nama, ket_ipk, ket_penghasilan, ket_tanggungan, ket_smt) VALUES('$nama[$a]','$textipk[$a]','$textpenghasilan[$a]','$texttanggungan[$a]','$textsmt[$a]')");

                                }

                                for($a=1;$a<=$jumlah;$a++) {
                                    
                                    $nama[$a] = $_POST['namasiswa'.$a];
                                    $gapipk[$a] = $nilaiipk[$a] - 3;
                                    $gappenghasilan[$a] = $nilaipenghasilan[$a] - 3;
                                    $gaptanggungan[$a] = $nilaitanggungan[$a] - 3;
                                    $gapsmt[$a] = $nilaismt[$a] - 2;

                                    $sql = $conn->query("INSERT INTO gapsiswa (nama, gapipk, gappenghasilan, gaptanggungan, gapsmt) VALUES('$nama[$a]','$gapipk[$a]','$gappenghasilan[$a]','$gaptanggungan[$a]','$gapsmt[$a]')");

                                }

                                for($a=1;$a<=$jumlah;$a++) {

                                    if ($gapipk[$a] == "0"){
                                        $bobotipk[$a] = "5";
                                    } elseif ($gapipk[$a] == "1") {
                                        $bobotipk[$a] = "4.5";
                                    } elseif ($gapipk[$a] == "-1") {
                                        $bobotipk[$a] = "4";
                                    } elseif ($gapipk[$a] == "2") {
                                        $bobotipk[$a] = "3.5";
                                    } elseif ($gapipk[$a] == "-2") {
                                        $bobotipk[$a] = "3";
                                    } elseif ($gapipk[$a] == "3") {
                                        $bobotipk[$a] = "2.5";
                                    } elseif ($gapipk[$a] == "-3") {
                                        $bobotipk[$a] = "2";
                                    } elseif ($gapipk[$a] == "4") {
                                        $bobotipk[$a] = "1.5";
                                    } else {
                                        $bobotipk[$a] = "1";
                                    }

                                    if ($gappenghasilan[$a] == "0"){
                                        $bobotpenghasilan[$a] = "5";
                                    } elseif ($gappenghasilan[$a] == "1") {
                                        $bobotpenghasilan[$a] = "4.5";
                                    } elseif ($gappenghasilan[$a] == "-1") {
                                        $bobotpenghasilan[$a] = "4";
                                    } elseif ($gappenghasilan[$a] == "2") {
                                        $bobotpenghasilan[$a] = "3.5";
                                    } elseif ($gappenghasilan[$a] == "-2") {
                                        $bobotpenghasilan[$a] = "3";
                                    } elseif ($gappenghasilan[$a] == "3") {
                                        $bobotpenghasilan[$a] = "2.5";
                                    } elseif ($gappenghasilan[$a] == "-3") {
                                        $bobotpenghasilan[$a] = "2";
                                    } elseif ($gappenghasilan[$a] == "4") {
                                        $bobotpenghasilan[$a] = "1.5";
                                    } else {
                                        $bobotpenghasilan[$a] = "1";
                                    }

                                    if ($gaptanggungan[$a] == "0"){
                                        $bobottanggungan[$a] = "5";
                                    } elseif ($gaptanggungan[$a] == "1") {
                                        $bobottanggungan[$a] = "4.5";
                                    } elseif ($gaptanggungan[$a] == "-1") {
                                        $bobottanggungan[$a] = "4";
                                    } elseif ($gaptanggungan[$a] == "2") {
                                        $bobottanggungan[$a] = "3.5";
                                    } elseif ($gaptanggungan[$a] == "-2") {
                                        $bobottanggungan[$a] = "3";
                                    } elseif ($gaptanggungan[$a] == "3") {
                                        $bobottanggungan[$a] = "2.5";
                                    } elseif ($gaptanggungan[$a] == "-3") {
                                        $bobottanggungan[$a] = "2";
                                    } elseif ($gaptanggungank[$a] == "4") {
                                        $bobottanggungan[$a] = "1.5";
                                    } else {
                                        $bobottanggungan[$a] = "1";
                                    }

                                    if ($gapsmt[$a] == "0"){
                                        $bobotsmt[$a] = "5";
                                    } elseif ($gapsmt[$a] == "1") {
                                        $bobotsmt[$a] = "4.5";
                                    } elseif ($gapsmt[$a] == "-1") {
                                        $bobotsmt[$a] = "4";
                                    } elseif ($gapsmt[$a] == "2") {
                                        $bobotsmt[$a] = "3.5";
                                    } elseif ($gapsmt[$a] == "-2") {
                                        $bobotsmt[$a] = "3";
                                    } elseif ($gapsmt[$a] == "3") {
                                        $bobotipk[$a] = "2.5";
                                    } elseif ($gapsmt[$a] == "-3") {
                                        $bobotsmt[$a] = "2";
                                    } elseif ($gapsmt[$a] == "4") {
                                        $bobotsmt[$a] = "1.5";
                                    } else {
                                        $bobotsmt[$a] = "1";
                                    }

                                    $ncfsiswa[$a] = (($bobotipk[$a]) + ($bobotpenghasilan[$a]))/2;
                                    $nsfsiswa[$a] = (($bobottanggungan[$a]) + ($bobotsmt[$a]))/2;
                                    $hasilsiswa[$a] = (0.6 * $ncfsiswa[$a]) + (0.4 * $nsfsiswa[$a]);

                                    $sql = $conn->query("INSERT INTO hasilsiswa (nama, bobotipk, bobotpenghasilan, bobottanggungan, bobotsmt, ncf, nsf, hasil) VALUES('$nama[$a]','$bobotipk[$a]','$bobotpenghasilan[$a]','$bobottanggungan[$a]','$bobotsmt[$a]','$ncfsiswa[$a]','$nsfsiswa[$a]','$hasilsiswa[$a]')");

                                }

                    ?>

                    <!-- TUTUP -->
                    <?php
                            }
                        }
                    ?>
                    <!-- /TUTUP -->
                    <br><br>
                    <!-- Table -->
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">Nama</th>
                                  <th scope="col">IPK</th>
                                  <th scope="col">Penghasilan Orang tua</th>
                                  <th scope="col">Tanggungan Orang tua</th>
                                  <th scope="col">Semester</th>
                                  <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $query1 = $conn->query("SELECT * FROM keterangansiswa");
                                    $a = 1;
                                    while($data = $query1->fetch_array()){
                                ?>
                                <tr>
                                    <th scope="row"><?php echo $a; ?></th>
                                    <td><?php echo $data["nama"];?></td>
                                    <td><?php echo $data["ket_ipk"];?></td>
                                    <td><?php echo $data["ket_penghasilan"];?></td>
                                    <td><?php echo $data["ket_tanggungan"];?></td>
                                    <td><?php echo $data["ket_smt"];?></td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button type="button" class="btn btn-warning" 
                                            onclick="window.location.href='<?php echo base_url ?>delete.php?id=<?php echo $data['nama']; ?>'">Hapus</button>
                                        </div>
                                    </td>
                                </tr>
                                <?php $a++; } ?>
                                </tbody>
                        </table>
                    </div>
                    <!-- /Tabel -->


                    <br><br>

                    <h3 class="box-title">Bobot data Mahasiswa</h3>
                    <!-- Table -->
                    <form  role="form" method="post" action="hasil.php" class="form-inline">
                        <table class="table">
                            <thead class="thead-dark">
                                <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">Nama</th>
                                  <th scope="col">IPK</th>
                                  <th scope="col">Penghasilan Orang tua</th>
                                  <th scope="col">Tanggungan Orang tua</th>
                                  <th scope="col">Semester</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    $query = $conn->query("SELECT * FROM siswa");
                                    $a = 1;
                                    while($data = $query->fetch_array()){
                                ?>
                                <tr>
                                  <th scope="row"><?php echo $a; ?></th>
                                  <td><?php echo $data["nama"];?></td>
                                  <td><?php echo $data["ipk"];?></td>
                                  <td><?php echo $data["penghasilan"];?></td>
                                  <td><?php echo $data["tanggungan"];?></td>
                                  <td><?php echo $data["semester"];?></td>
                                </tr>
                                <?php $a++; } ?>
                            </tbody>
                            <thead class="thead-dark">
                                <tr>
                                  <th scope="col">GAP</th>
                                  <th scope="col"></th>
                                  <th scope="col">3</th>
                                  <th scope="col">3</th>
                                  <th scope="col">3</th>
                                  <th scope="col">2</th>
                                </tr>
                            </thead>
                        </table>
                        <!-- <button type="submit" name="hitunggap" class="btn btn-primary mb-2">Hitung</button> -->
                    </form>
                    <!-- /Tabel -->

            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- modal -->
<div class="modal fade" id="modal_form"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-material" id="form">
            <div class="form-group">
                <label class="col-md-12">Nama Donatur</label>
                <div class="col-md-12">
                     <input type="hidden" id="id" name="id"/> 
                    <input type="text" name="nama" id="nama" placeholder="Nama Donatur" class="form-control form-control-line">
                    <span class="help-block"></span>
                </div>
                    
            </div>
            <div class="form-group">
                <label for="jumlah" class="col-md-12">Jumlah</label>
                <div class="col-md-12">
                    <input type="number" placeholder="Jumlah" class="form-control form-control-line" name="jumlah" id="jumlah"> 
                    <span class="help-block"></span>
                </div>
            </div>
        
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="btnSave" class="btn btn-success" onclick="save()">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- endmodal -->

<script>
$('#dataku').dataTable();

let save_method;
function tambah() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); 
    $('.modal-title').text('Tambah Dana Dana Masuk'); 
}
function save(){
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    let url;

    if(save_method == 'add') {
        url = "server_side/kas_masuk/tambah_kas_masuk.php";
    } else {
        url = "server_side/kas_masuk/edit_kas_masuk.php";
    }

    // ajax adding data to database

    let formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                 // delay 1 detik
                  setTimeout(function() { $('#kontenku').load('page/kas_masuk.php'); }, 1000);
                
            }else{
                for (let i = 0; i < data.inputerror.length; i++){
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}
function edit_kas_masuk(id){
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string


    //Ajax Load data from ajax
    $.ajax({
        url : "server_side/kas_masuk/get_data_masuk.php?id="+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('#id').val(data.id);
            $('#nama').val(data.nama);
            $('#jumlah').val(data.jumlah);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Dana Dana Masuk '); // Set title to Bootstrap modal title


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function hapus_kas_masuk(id)
{
    if(confirm('Kamu Yakin hapus data ini?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "server_side/kas_masuk/hapus_kas_masuk.php?id="+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                setTimeout(function() { $('#kontenku').load('page/kas_masuk.php'); }, 1000);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}
function laporan() {
    $('#kontenku').load('page/laporan_kas_masuk.php');
}

</script>