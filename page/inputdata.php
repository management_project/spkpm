<?php 
require_once '../koneksi/conn.php'; 
$query = $conn->query("SELECT * FROM kas_masuk ORDER BY tanggal DESC");
?>
<div class="container-fluid">
    <div class="row bg-title">
        <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">Data Mahasiswa</h4> </div>
        <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <!-- <ol class="breadcrumb">
                <li><a href="#">Dana Masuk</a></li>
            </ol> -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
     <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
                <div class="row">
                    <div class="col-sm-6">
                        <h3 class="box-title">Records data Mahasiswa</h3>
                    </div>
                             <form  role="form" method="post" action="<?php echo base_url ?>tambah_mahasiswa.php"><br>
                             <?php
                              session_start();
                                $jumlah = 1;
                                $a=1;
                             ?>

                            <div class="form-group">
                                <label><b>Mahasiswa ke <?php echo$a; ?></b></label><br>
                                <label for="exampleFormControlInput1">Nama Mahasiswa</label>
                                <input class="form-control" placeholder="Nama Siswa" name="namasiswa<?php echo $a; ?>" autocomplete="off">
                              </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">IPK</label>
                                <select name="ipk<?php echo $a; ?>" class="form-control" id="exampleFormControlSelect1">
                                    <option>----Select an option----</option>
                                    <option value="1"><2,5</option>
                                    <option value="2">>2,5 - <=3</option>
                                    <option value="3">>3 - <=3,5</option>
                                    <option value="4">>3,5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect2">Penghasilan Orang Tua</label>
                                <select name="penghasilan<?php echo $a; ?>" class="form-control" id="exampleFormControlSelect2">
                                    <option>----Select an option----</option>
                                    <option value="4"><=1.000.000</option>
                                    <option value="3">>1.000.000 - <=3.000.000</option>
                                    <option value="2">>3.000.000 - <=5.000.000</option>
                                    <option value="1">>=5.000.000</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect2">Tanggungan Orang Tua</label>
                                <select name="tanggungan<?php echo $a; ?>" class="form-control" id="exampleFormControlSelect2">
                                    <option>----Select an option----</option>
                                    <option value="1">Jumlah 1</option>
                                    <option value="2">Jumlah 2</option>
                                    <option value="3">Jumlah 3</option>
                                    <option value="4">Jumlah >3</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect2">Semester</label>
                                <select name="smt<?php echo $a; ?>" class="form-control" id="exampleFormControlSelect2">
                                    <option>----Select an option----</option>
                                    <option value="0"><=2 / >8</option>
                                    <option value="1">3</option>
                                    <option value="2">4</option>
                                    <option value="3">5, 6</option>
                                    <option value="4">7, 8</option>
                                </select>
                            </div>
                            <br>
                                <button type="submit" name="submitform" class="btn btn-primary">Submit</button><br>
                            </form>
    
                </div>
                

    <!-- Hapus Record -->
                    
            </div>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- modal -->
<div class="modal fade" id="modal_form"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Modal title</h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal form-material" id="form">
            <div class="form-group">
                <label class="col-md-12">Nama Donatur</label>
                <div class="col-md-12">
                     <input type="hidden" id="id" name="id"/> 
                    <input type="text" name="nama" id="nama" placeholder="Nama Donatur" class="form-control form-control-line">
                    <span class="help-block"></span>
                </div>
                    
            </div>
            <div class="form-group">
                <label for="jumlah" class="col-md-12">Jumlah</label>
                <div class="col-md-12">
                    <input type="number" placeholder="Jumlah" class="form-control form-control-line" name="jumlah" id="jumlah"> 
                    <span class="help-block"></span>
                </div>
            </div>
        
        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        <button type="button" id="btnSave" class="btn btn-success" onclick="save()">Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- endmodal -->

<script>
$('#dataku').dataTable();

let save_method;
function tambah() {
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); 
    $('.modal-title').text('Tambah Dana Dana Masuk'); 
}
function save(){
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    let url;

    if(save_method == 'add') {
        url = "server_side/kas_masuk/tambah_kas_masuk.php";
    } else {
        url = "server_side/kas_masuk/edit_kas_masuk.php";
    }

    // ajax adding data to database

    let formData = new FormData($('#form')[0]);
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form').modal('hide');
                 // delay 1 detik
                  setTimeout(function() { $('#kontenku').load('page/kas_masuk.php'); }, 1000);
                
            }else{
                for (let i = 0; i < data.inputerror.length; i++){
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
        }
    });
}
function edit_kas_masuk(id){
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string


    //Ajax Load data from ajax
    $.ajax({
        url : "server_side/kas_masuk/get_data_masuk.php?id="+id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('#id').val(data.id);
            $('#nama').val(data.nama);
            $('#jumlah').val(data.jumlah);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Dana Dana Masuk '); // Set title to Bootstrap modal title


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function hapus_kas_masuk(id)
{
    if(confirm('Kamu Yakin hapus data ini?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "server_side/kas_masuk/hapus_kas_masuk.php?id="+id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                setTimeout(function() { $('#kontenku').load('page/kas_masuk.php'); }, 1000);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}
function laporan() {
    $('#kontenku').load('page/laporan_kas_masuk.php');
}

</script>